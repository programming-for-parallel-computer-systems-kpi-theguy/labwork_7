Laboratory work #7 for Programming for Parallel Computer Systems.

Purpose for the work: To study the Rende-vouz mechanism, that was made for programming language - Ada, and works with parallel computer system with distributed memory.
Programming language: Ada.
Parallel computer system topology - hypercube (n=3).
Used technologies: To send information between nodes of the distributed parallel computer system, Ada gives programmer opportunity to communicate with entry fields of the threads. For receiving them it's only comes to accept the connection through the entry field.

Controling of the program:
variable N stands for the size of Matrixes and Vectors respectively.
Program works for 8 threads, third one outputs the result.