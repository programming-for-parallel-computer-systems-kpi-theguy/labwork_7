-------------------PfPCS----------------------
-----------------Labwork #7-------------------
---------------Ada. Randevouz-----------------
----------------------------------------------
----Task: MA = min(Z) * MO + d * (MK * MS)----
----------------------------------------------
----Author: Butskiy Yuriy, IO-52 group--------
----Date: 22.05.2018--------------------------
----------------------------------------------

with Ada.Text_IO, Ada.Integer_Text_IO, Ada.Calendar;
use Ada.Text_IO, Ada.Integer_Text_IO, Ada.Calendar;
with data;

procedure Lab7 is

   N: Integer := 2000;
   P: Integer := 8;

   package using_data is new data(N, P);
   use using_data;

   procedure tasks is

    task T1 is
        pragma Storage_Size(1000000);
        entry InA_1(b: in Integer);
        entry InA_5(b: out Integer);
    end T1;

	task T2 is
        pragma Storage_Size(1000000);
        entry Inp1_2(MB, MC: in Matrix4H);
        entry InA_2(b: in Integer);
        entry InA_6(b: out Integer);
    end T2;

    task T3 is
        pragma Storage_Size(1000000);
        entry Inp3_1(MB, MC: in Matrix2H; MD: out Matrix);
        entry OutA_1(b: in Integer);
        entry OutA_4(b: in Integer);
        entry InA_7(b: out Integer);
        entry Res_1(MB: in Matrix2H);
        entry Res_4(MB: in Matrix4H);
    end T3;

    task T4 is
        pragma Storage_Size(1000000);
        entry Inp3_4(MB: in Matrix);
        entry Inp1_4(MB, MC: in Matrix2H; MD: out Matrix);
        entry OutA_2(b: in Integer);
        entry InA_4(b: in Integer);
        entry InA_8(b: out Integer);
        entry Res_2(MB: in Matrix2H);
    end T4;

    task T5 is
        pragma Storage_Size(1000000);
        entry Inp13_5(MB, MC: in MatrixH; MD: in Matrix; B: out VectorH; c: out Integer);
        entry Inp8_5(B: in Vector2H; c: in Integer);
        entry OutA_5(b: out Integer);
        entry Res_5(MB: out MatrixH);
    end T5;

    task T6 is
        pragma Storage_Size(1000000);
        entry Inp13_6(MB, MC: in MatrixH; MD: in Matrix; B: out VectorH; c: out Integer);
        entry Inp8_6(B: in Vector2H; c: in Integer);
        entry OutA_6(b: out Integer);
        entry Res_6(MB: out MatrixH);
    end T6;

    task T7 is
        pragma Storage_Size(1000000);
        entry Inp13_7(MB, MC: in MatrixH; MD: in Matrix; B: out VectorH; c: out Integer);
        entry Inp8_7(B: in Vector4H; c: in Integer);
        entry OutA_7(b: out Integer);
        entry Res_7(MB: out MatrixH);
    end T7;

    task T8 is
        pragma Storage_Size(1000000);
        entry Inp13_8(MB, MC: in MatrixH; MD: in Matrix; B: out VectorH; c: out Integer);
        entry OutA_8(b: out Integer);
        entry Res_8(MB: out MatrixH);
    end T8;

    task body T1 is
        d: Integer;
        a: Integer;
        ae: Integer;
        Z: VectorH;
        MO: Matrix;
        MO3: Matrix2H;
        MO2: Matrix4H;
        MS: Matrix;
        MS3: Matrix2H;
        MS2: Matrix4H;
        MK: Matrix;
        MA: Matrix2H;
      begin
        Put_Line("T1 started");

        --Input values of MO, MS
        Input_Matrix(MO);
        Input_Matrix(MS);
        --Send MO4h, MS4h to T2
        MO2(1..H) := MO(H+1..2*H);
        MO2(H+1..2*H) := MO(3*H+1..4*H);
        MO2(2*H+1..3*H) := MO(5*H+1..6*H);
        MO2(3*H+1..4*H) := MO(7*H+1..N);

        MS2(1..H) := MS(H+1..2*H);
        MS2(H+1..2*H) := MS(3*H+1..4*H);
        MS2(2*H+1..3*H) := MS(5*H+1..6*H);
        MS2(3*H+1..4*H) := MS(7*H+1..N);
        T2.Inp1_2(MO2, MS2);

        --Receive MK, and Send MO2h, MS2h to T3
        MO3(1..H) := MO(2*H+1..3*H);
        MO3(H+1..2*H) := MO(6*H+1..7*H);

        MS3(1..H) := MS(2*H+1..3*H);
        MS3(H+1..2*H) := MS(6*H+1..7*H);
        T3.Inp3_1(MO3,MS3,MK);

        --Send MOh, MSh, MK and Receive Zh, d from T5
        T5.Inp13_5(MO(4*H+1..5*H),MS(4*H+1..5*H),MK,Z,d);

        --Calculation of local min
		a := Min_Vector(Z);
		
		--Receive a5 from T5
		T5.OutA_5(ae);
		
		--Calculation of min step 2
		a := Integer'Min(a, ae);
		
		--Send a1 to T3
		T3.OutA_1(a);
		
		--Receive a from T3
		accept InA_1(b: in Integer) do
			a := b;
		end InA_1;
		
		--Send a to T5
		accept InA_5(b: out Integer) do
			b := a;
		end InA_5;
		
		--Calculation of math function
		Sum_Matrixes(Multiply_Matrix_Integer(MO(1..H),a),Multiply_Matrix_Integer(Multiply_Matrixes(MS(1..H),MK),d),MA(1..H));
		
		--Receive MAh from T5
		T5.Res_5(MA(H+1..2*H));
		
		--Send MA2h to T3
		T3.Res_1(MA);
		
		Put_Line("T1 finished");
	end T1;
	
	task body T2 is
		a: Integer;
		ae: Integer;
		d: Integer;
		Z: VectorH;
		MO: Matrix4H;
		MO4: Matrix2H;
		MS: Matrix4H;
		MS4: Matrix2H;
		MK: Matrix;
		MA: Matrix2H;
	begin
		Put_Line("T2 started");
		
		--Receive MO4h, MS4h from T1
		accept Inp1_2(MB, MC: in Matrix4H) do
			MO := MB;
			MS := MC;
		end Inp1_2;
		
		--Send MO2h, MS2h and Receive MK from T4
		MO4(1..H) := MO(H+1..2*H);
        MO4(H+1..2*H) := MO(3*H+1..4*H);
		
		MS4(1..H) := MO(H+1..2*H);
        MS4(H+1..2*H) := MO(3*H+1..4*H);
		T4.Inp1_4(MO4,MS4,MK);
		
		--Receive Zh, d and Send MOh, MSh, MK to T6
		T6.Inp13_6(MO(2*H+1..3*H),MS(2*H+1..3*H),MK,Z,d);
		
		--Calculation of local min
		a := Min_Vector(Z);
		
		--Receive a6 from T6
		T6.OutA_6(ae);
		
		--Calculation of min step 2
		a := Integer'Min(a, ae);
		
		--Send a2 to T4
		T4.OutA_2(a);
		
		--Receive a from T4
		accept InA_2(b: in Integer) do
			a := b;
		end InA_2;
		
		--Send a to T6
		accept InA_6(b: out Integer) do
			b := a;
		end InA_6;
		
		--Calculation of math function
		Sum_Matrixes(Multiply_Matrix_Integer(MO(1..H),a),Multiply_Matrix_Integer(Multiply_Matrixes(MS(1..H),MK),d),MA(1..H));
		
		--Receive MAh from T6
		T6.Res_6(MA(H+1..2*H));
		
		--Send MA2h to T4
		T4.Res_2(MA);
		
		Put_Line("T2 finished");
	end T2;
	
	task body T3 is
		a: Integer;
		ae: Integer;
		d: Integer;
		Z: VectorH;
		MA: Matrix;
		MO: Matrix2H;
		MS: Matrix2H;
		MK: Matrix;
	begin
		Put_Line("T3 started");
		
		--Input values of MK
		Input_Matrix(MK);
		
		--Send MK to T4
		T4.Inp3_4(MK);
		
		--Send MK and Receive MO2h, MS2h from T1
		accept Inp3_1(MB, MC: in Matrix2H; MD: out Matrix) do
			MO := MB;
			MS := MC;
			MD := MK;
		end Inp3_1;
		
		--Send MOh, MSh, MK and Receive Zh, d from T7
		T7.Inp13_7(MO(H+1..2*H),MS(H+1..2*H),MK,Z,d);
		
		--Calculation of local min
		a := Min_Vector(Z);
		
		--Receive a7 from T7
		T7.OutA_7(ae);
		
		--Calculation of min step 2
		a := Integer'Min(a, ae);
		
		--Receive a1 from T1
		accept OutA_1(b: in Integer) do
			ae := b;
		end OutA_1;
		
		--Calculation of min step 3
		a := Integer'Min(a, ae);
		
		--Receive a4 from T4
		accept OutA_4(b: in Integer) do
			ae := b;
		end OutA_4;
		
		--Calculation of min step 4
		a := Integer'Min(a, ae);
		
		--Send a to T4
		T4.InA_4(a);
		
		--Send a to T1
		T1.InA_1(a);
		
		--Send a to T7
		accept InA_7(b: out Integer) do
			b := a;
		end InA_7;
		
		--Calculation of math function
		Sum_Matrixes(Multiply_Matrix_Integer(MO(1..H),a),Multiply_Matrix_Integer(Multiply_Matrixes(MS(1..H),MK),d),MA(2*H+1..3*H));
		
		--Receive MAh from T7
		T7.Res_7(MA(6*H+1..7*H));
		
		--Receive MA2h from T1
		accept Res_1(MB: in Matrix2H) do
			MA(1..H) := MB(1..H);
			MA(4*H+1..5*H) := MB(H+1..2*H);
		end Res_1;
		
		--Receive MA4h from T4
		accept Res_4(MB: in Matrix4H) do
			MA(H+1..2*H) := MB(1..H);
			MA(3*H+1..4*H) := MB(H+1..2*H);
			MA(5*H+1..6*H) := MB(2*H+1..3*H);
			MA(7*H+1..8*H) := MB(3*H+1..4*H);
		end Res_4;
		
		--Output the result
		Output_Matrix(MA);
		
		Put_Line("T3 finished");
	end T3;
	
	task body T4 is
		a: Integer;
		ae: Integer;
		d: Integer;
		Z: VectorH;
		MO: Matrix2H;
		MS: Matrix2H;
		MK: Matrix;
		MA: Matrix4H;
	begin
		Put_Line("T4 started");
		
		--Receive MK from T3
		accept Inp3_4(MB: in Matrix) do
			MK := MB;
		end Inp3_4;
		
		--Receive MO2h, MS2h and Send MK to T2
		accept Inp1_4(MB, MC: in Matrix2H; MD: out Matrix) do
			MO := MB;
			MS := MC;
			MD := MK;
		end Inp1_4;
		
		--Send MOh, MSh, MK and Receive Zh, d from T8
		T8.Inp13_8(MO(H+1..2*H),MS(H+1..2*H),MK,Z,d);
		
		--Calculation of local min
		a := Min_Vector(Z);
		
		--Receive a8 from T8
		T8.OutA_8(ae);
		
		--Calculation of min step 2
		a := Integer'Min(a, ae);
		
		--Receive a2 from T2
		accept OutA_2(b: in Integer) do
			ae := b;
		end OutA_2;
		
		--Calculation of min step 3
		a := Integer'Min(a, ae);
		
		--Send a4 to T3
		T3.OutA_4(a);
		
		--Receive a from T3
		accept InA_4(b: in Integer) do
			a := b;
		end InA_4;
		
		--Send a to T2
		T2.InA_2(a);
		
		--Send a to T8
		accept InA_8(b: out Integer) do
			b := a;
		end InA_8;
		
		--Calculation of math function
		Sum_Matrixes(Multiply_Matrix_Integer(MO(1..H),a),Multiply_Matrix_Integer(Multiply_Matrixes(MS(1..H),MK),d),MA(H+1..2*H));
		
		--Receive MAh from T8
		T8.Res_8(MA(3*H+1..4*H));
		
		--Receive MA2h from T2
		accept Res_2(MB: in Matrix2H) do
			MA(1..H) := MB(1..H);
			MA(2*H+1..3*H) := MB(H+1..2*H);
		end Res_2;
		
		--Send MA4h to T3
		T3.Res_4(MA);
		
		Put_Line("T4 finished");
	end T4;
	
	task body T5 is
		a: Integer;
		d: Integer;
		Z: Vector2H;
		MO: MatrixH;
		MS: MatrixH;
		MK: Matrix;
		MA: MatrixH;
	begin
		Put_Line("T5 started");
		
		--Receive Z2h, d from T7
		accept Inp8_5(B: in Vector2H; c: in Integer) do
			Z := B;
			d := c;
		end Inp8_5;
		
		--Receive MOh, MSh, MK and Send Zh, d to T1
		accept Inp13_5(MB, MC: in MatrixH; MD: in Matrix; B: out VectorH; c: out Integer) do
			MO := MB;
			MS := MC;
			MK := MD;
			B := Z(1..H);
			c := d;
		end Inp13_5;
		
		--Calculation of local min
		a := Min_Vector(Z(H+1..2*H));
		
		--Send a5 to T1
		accept OutA_5(b: out Integer) do
			b := a;
		end OutA_5;
		
		--Receive a from T1
		T1.InA_5(a);
		
		--Calculation of math function
		Sum_Matrixes(Multiply_Matrix_Integer(MO,a),Multiply_Matrix_Integer(Multiply_Matrixes(MS,MK),d),MA);
		
		--Send MAh to T1
		accept Res_5(MB: out MatrixH) do
			MB := MA;
		end Res_5;
		
		Put_Line("T5 finished");
	end T5;
	
	task body T6 is
		a: Integer;
		d: Integer;
		Z: Vector2H;
		MO: MatrixH;
		MS: MatrixH;
		MK: Matrix;
		MA: MatrixH;
	begin
		Put_Line("T6 started");
		
		--Receive Z2h, d from T8
		accept Inp8_6(B: in Vector2H; c: in Integer) do
			Z := B;
			d := c;
		end Inp8_6;
		
		--Receive MOh, MSh, MK and Send Zh, d to T1
		accept Inp13_6(MB, MC: in MatrixH; MD: in Matrix; B: out VectorH; c: out Integer) do
			MO := MB;
			MS := MC;
			MK := MD;
			B := Z(1..H);
			c := d;
		end Inp13_6;
		
		--Calculation of local min
		a := Min_Vector(Z(H+1..2*H));
		
		--Send a6 to T2
		accept OutA_6(b: out Integer) do
			b := a;
		end OutA_6;
		
		--Receive a from T2
		T2.InA_6(a);
		
		--Calculation of math function
		Sum_Matrixes(Multiply_Matrix_Integer(MO,a),Multiply_Matrix_Integer(Multiply_Matrixes(MS,MK),d),MA);
		
		--Send MAh to T2
		accept Res_6(MB: out MatrixH) do
			MB := MA;
		end Res_6;
		
		Put_Line("T6 finished");
	end T6;
	
	task body T7 is
		a: Integer;
		d: Integer;
		Z: Vector4H;
		Z5: Vector2H;
		MO: MatrixH;
		MS: MatrixH;
		MK: Matrix;
		MA: MatrixH;
	begin
		Put_Line("T7 started");
		
		--Receive Z4h, d from T8
		accept Inp8_7(B: in Vector4H; c: in Integer) do
			Z := B;
			d := c;
		end Inp8_7;
		
		--Send Z2h, d to T5
		Z5(1..H) := Z(1..H);
		Z5(H+1..2*H) := Z(2*H+1..3*H);
		T5.Inp8_5(Z5,d);
		
		--Receive MOh, MSh, MK and Send Zh, d to T3
		accept Inp13_7(MB, MC: in MatrixH; MD: in Matrix; B: out VectorH; c: out Integer) do
			MO := MB;
			MS := MC;
			MK := MD;
			B := Z(H+1..2*H);
			c := d;
		end Inp13_7;
		
		--Calculation of local min
		a := Min_Vector(Z(3*H+1..4*H));
		
		--Send a7 to T3
		accept OutA_7(b: out Integer) do
			b := a;
		end OutA_7;
		
		--Receive a from T3
		T3.InA_7(a);
		
		--Calculation of math function
		Sum_Matrixes(Multiply_Matrix_Integer(MO,a),Multiply_Matrix_Integer(Multiply_Matrixes(MS,MK),d),MA);
		
		--Send MAh to T3
		accept Res_7(MB: out MatrixH) do
			MB := MA;
		end Res_7;
		
		Put_Line("T7 finished");
	end T7;
	
	task body T8 is
		a: Integer;
		d: Integer;
		Z: Vector;
		Z7: Vector4H;
		Z6: Vector2H;
		MO: MatrixH;
		MS: MatrixH;
		MK: Matrix;
		MA: MatrixH;
	begin
		Put_Line("T8 started");
		
		--Input values of Z and d
		Input_Vector(Z);
		Input_Integer(d);
		
		--Send Z4h, d to T7
		Z7(1..H) := Z(1..H);
		Z7(H+1..2*H) := Z(2*H+1..3*H);
		Z7(2*H+1..3*H) := Z(4*H+1..5*H);
		Z7(3*H+1..4*H) := Z(6*H+1..7*H);
		T7.Inp8_7(Z7,d);
		
		--Send Z2h, d to T6
		Z6(1..H) := Z(H+1..2*H);
		Z6(H+1..2*H) := Z(5*H+1..6*H);
		T6.Inp8_6(Z6,d);
		
		--Receive MOh, MSh, MK and Send Zh, d to T1
		accept Inp13_8(MB, MC: in MatrixH; MD: in Matrix; B: out VectorH; c: out Integer) do
			MO := MB;
			MS := MC;
			MK := MD;
			B := Z(3*H+1..4*H);
			c := d;
		end Inp13_8;
		
		--Calculation of local min
		a := Min_Vector(Z(7*H+1..N));
		
		--Send a8 to T4
		accept OutA_8(b: out Integer) do
			b := a;
		end OutA_8;
		
		--Receive a from T4
		T4.InA_8(a);
		
		--Calculation of math function
		Sum_Matrixes(Multiply_Matrix_Integer(MO,a),Multiply_Matrix_Integer(Multiply_Matrixes(MS,MK),d),MA);
		
		--Send MAh to T4
		accept Res_8(MB: out MatrixH) do
			MB := MA;
		end Res_8;
		
		Put_Line("T8 finished");
      end T8;
    
   begin
      null;
   end tasks;
   
begin
   tasks;
end Lab7;
