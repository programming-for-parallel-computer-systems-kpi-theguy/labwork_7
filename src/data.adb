-------------------PfPCS----------------------
-----------------Labwork #7-------------------
---------------Ada. Randevouz-----------------
----------------------------------------------
----Task: MA = min(Z) * MO + d * (MK * MS)----
----------------------------------------------
----Author: Butskiy Yuriy, IO-52 group--------
----Date: 22.05.2018--------------------------
----------------------------------------------

with Ada.Text_IO, Ada.Integer_Text_IO;
use Ada.Text_IO, Ada.Integer_Text_IO;

package body data is
	
procedure Input_Integer(a: out Integer) is

begin
    a := 1;
end Input_Integer;

procedure Input_Vector(A: out Vector) is

begin
    for i in 1..N loop
        A(i):=1;
    end loop;
end Input_Vector;

procedure Input_Matrix(MA: out Matrix) is

begin
    for i in 1..N loop
        for j in 1..N loop
			MA(i)(j):=1;
        end loop;
    end loop;
end Input_Matrix;

procedure Output_Matrix(MA: in Matrix) is

begin
    if (N < 20) then
        New_Line;
        for i in 1..N loop
			for j in 1..N loop
				Put(MA(i)(j));
            end loop;
            New_Line;
        end loop;
        New_Line;
    end if;
end Output_Matrix;

function Multiply_Matrixes(MA: in MatrixH; MB: in Matrix) return MatrixH is
   	cell: Integer;
	result: MatrixH;
begin
	for i in 1..H loop
		for j in 1..N loop
			cell := 0;
			for l in 1..N loop
                cell := cell + MA(i)(l) * MB(l)(j);
			end loop;
            result(i)(j) := cell;
		end loop;
	end loop;
	return result;
end Multiply_Matrixes;

function Multiply_Matrix_Integer(MA: in MatrixH; a: in Integer) return MatrixH is
	result: MatrixH;
begin
	for i in 1..H loop
        for j in 1..N loop       
			result(i)(j) := a * MA(i)(j);
		end loop;
	end loop;
	return result;
end Multiply_Matrix_Integer;

procedure Sum_Matrixes(MA, MB: in MatrixH; MC: out MatrixH) is
begin
	for i in 1..H loop
		for j in 1..N loop
			MC(i)(j) := MA(i)(j) + MB(i)(j);
		end loop;
	end loop;
end Sum_Matrixes;

function Min_Vector(A: in VectorH) return Integer is
	result: Integer;
begin
	result := A(1);
      for i in 1..H loop
		if A(i) < result then
			result := A(i);
		end if;
	end loop;
	return result;
end Min_Vector;

end data;
