-------------------PfPCS----------------------
-----------------Labwork #7-------------------
---------------Ada. Randevouz-----------------
----------------------------------------------
----Task: MA = min(Z) * MO + d * (MK * MS)----
----------------------------------------------
----Author: Butskiy Yuriy, IO-52 group--------
----Date: 22.05.2018--------------------------
----------------------------------------------

generic
   N: in Integer;
   P: in Integer;
	package data is
		H: Integer := N/P;
		
		type Vector_Common is array(Integer range <>) of Integer;

        subtype VectorH is Vector_Common (1..H);
		subtype Vector2H is Vector_Common (1..2*H);
		subtype Vector4H is Vector_Common (1..4*H);
		subtype Vector is Vector_Common (1..N);
		
		type Matrix_Common is array(Integer range <>) of Vector;
		
		subtype MatrixH is Matrix_Common (1..H);
		subtype Matrix2H is Matrix_Common (1..2*H);
		subtype Matrix4H is Matrix_Common (1..4*H);
		subtype Matrix is Matrix_Common (1..N);

		--Input Integer, Vector, Matrix
		procedure Input_Vector (A: out Vector);
		procedure Input_Matrix (MA: out Matrix);
		procedure Input_Integer(a: out Integer);

		--Output Matrix
		procedure Output_Matrix (MA: in Matrix);

		--Multiplication functions
		function Multiply_Matrixes(MA: in MatrixH; MB: in Matrix) return MatrixH;
                function Multiply_Matrix_Integer(MA: in MatrixH; a: in Integer) return MatrixH;

		--Sum function
		procedure Sum_Matrixes(MA, MB: in MatrixH; MC: out MatrixH);

		--Min function
		function Min_Vector(A: in VectorH) return Integer;

		end data;
